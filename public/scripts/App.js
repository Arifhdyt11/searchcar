class App {
  constructor() {
    this.typeDriver = document.querySelector("#typeDriver");
    this.date = document.querySelector("#date");
    this.time = document.querySelector("#time");
    this.passenger = document.querySelector("#passenger");
    this.btn = document.querySelector(".submit");
    this.carsContainer = document.querySelector(".insert-card-cars");
    this.filter = document.querySelector("#pencarianmu");
    this.section = document.querySelector(".section-search");
  }

  // Btn Serach onclick
  async init() {
    if (this.btn !== null) {
      this.btn.onclick = await this.click; //Load function click
    } 
  }

  async loadFilter(filter) {
    const cars = await Database.loadCarsFilter(filter);
    Car.init(cars);
  }


  //After onClick
  click = async () => {
    let type = this.typeDriver.options[this.typeDriver.selectedIndex].value;
    let passenger = this.passenger.value;
    let date = this.date.value;
    let time = this.time.value;
    if (type.length !== 0 || date.length !== 0 || time.length !== 0) {
      if (passenger.length === 0) {
        passenger = 0;
      }
      date = new Date(this.date.value);
      await this.loadFilter({ type, passenger, date, time });
    }
    this.cardRender();
    this.addFilterText();
  };

  cardRender() {
    let card = "";
    if (Car.list.length !== 0) {
      Car.list.forEach((car) => {
        card += car.render();
      });
      this.carsContainer.innerHTML = card;
    } else {
      card = `<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                    <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path
                            d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                    </symbol>
                </svg>
                <div class="alert alert-danger d-flex align-items-center " role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                        <use xlink:href="#exclamation-triangle-fill" />
                    </svg>
                    <div class="text-center">
                        Mobil Tidak Ditemukan
                    </div>
                </div>`;
      this.carsContainer.innerHTML = card;
    }
  }

  addFilterText() {
    this.section.style.height = "142px"
    this.filter.style.display = "block";
    this.btn.innerHTML = "Edit";
  }

}
