class Car extends Component {
  static list = [];

  static init(cars) {
    // Mapping js
    this.list = cars.map((car) => new this(car));
  }

  constructor(props) {
    super(props);
    let { id, plate, manufacture, model, image, rentPerDay, capacity, description, transmission, available, type, year, options, specs, availableAt } = props;
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  // Inherritance Override
  render() {
    return `
          <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
            <div class="card-cars">
                  <img class="cars-img " src="${this.image}" alt="">
                  <div class="title-cars">
                      <h5>${this.manufacture}/${this.model}</h5>
                      <h3>Rp ${this.rentPerDay} / hari</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                          magna
                          aliqua.</p>
                  </div>
                  <div class="spec-cars  ">
                      <div class="spec-detail d-flex justify-content-start">
                          <img class="spec-img" src="img/fi_users.png" alt="">
                          <p class="align-self-center mb-0">${this.capacity} orang</p>
                      </div>
                      <div class="spec-detail d-flex justify-content-start">
                          <img class="spec-img" src="img/fi_settings.png" alt="">
                          <p class="align-self-center mb-0">${this.transmission}</p>
                      </div>
                      <div class="spec-detail d-flex justify-content-start">
                          <img class="spec-img" src="img/fi_calendar.png" alt="">
                          <p class="align-self-center mb-0">Tahun ${this.year}</p>
                      </div>
                  </div>
                  <div class="choose-cars">
                      <button class="btn btn-choose-cars">
                          <a href="#">Pilih Mobil</a>
                      </button>
                  </div>
            </div>
          </div>
    `;
  }
}

 

